library('rpart')
library('plyr')

# Tree test
testTreeConfigs <- function(train, test, type, prune, returnTree) {
  tree <- NULL
  
  if(type == "gini") {
    tree <- rpart(Class ~., data = train, parms = list(split = "gini"))
  }
  else {
    if(type == "info") {
      tree <- rpart(Class ~., data = train, parms = list(split = "information"))
    }
    else {
      print("Bad type")
      return(0)
    }
  }
  
  if(prune == 1) {
    tree <- prune(tree, cp = tree$cptable[which.min(tree$cptable[,"xerror"]),"CP"])
  }
  
  if(returnTree == 1) {
    return(tree)
  }
  else {
    pred_tree <- predict(tree, test, type = "class")
    return(mean(pred_tree == test$Class))
  }
}

# Cross validation
crossValidation <- function(dataset, folds, type, prune) {
  accuracies <- vector("numeric")
  data <- dataset
  
  # Save Classes as factors
  data$Class <- factor(data$Class)
  
  # sample from 1 to k, nrow times (the number of observations in the data)
  data$id <- sample(1:k, nrow(data), replace = TRUE)
  list <- 1:k
  
  # Creating progress bar
  progress.bar <- create_progress_bar("text")
  progress.bar$init(k)
  
  for (i in 1:k)
  {
    # remove rows with id i from dataframe to create training set
    # select rows with id i to create test set
    trainingset <- subset(data, id %in% list[-i])
    testset <- subset(data, id %in% c(i))

    #Evaluate classifier for k-th fold
    fold_accuracy <- testTreeConfigs(trainingset, testset, type, prune, 0)
    
    #Save the accuracy from the k-th fold
    accuracies <- append(accuracies, fold_accuracy)
    progress.bar$step()
  }
  
  #Calculate the final accuracy from k folds
  return(mean(accuracies))
}

#################
#               #
# Main function #
#               #
#################

dataset_1 <- read.csv("~/R/lab3/indianliverpatient_new.csv")
dataset_2 <- read.csv("~/R/lab3/cmc_new.csv")
dataset_3 <- read.csv("~/R/lab3/wine_new.csv")
dataset_1$Gender <- as.numeric(dataset_1$Gender)

# 10 - fold CV
k <- 10

types = c("info", "gini")
pruning = c(0, 1)
datasets = c(dataset_1, dataset_2, dataset_3)

for(type in types) {
  for(prune in pruning) {
    print(sprintf("Indian Liver Patient, %s, %d", type, prune))
    print(crossValidation(dataset_1, k, type, prune))
    tree <- testTreeConfigs(dataset_1, dataset_1, type, prune, 1)
    png(filename = sprintf("~/R/lab3/img/1_%s_%d.png", type, prune), width = 500, height = 700)
    plot(tree, uniform=TRUE,
         main = sprintf("Indian Liver Patient, %s, %d", type, prune))
    text(tree, use.n=TRUE, all=TRUE, cex=.8)
    dev.off()
    
    print(sprintf("Contraceptive Method Choice, %s, %d", type, prune))
    print(crossValidation(dataset_2, k, type, prune))
    tree <- testTreeConfigs(dataset_1, dataset_1, type, prune, 1)
    png(filename = sprintf("~/R/lab3/img/2_%s_%d.png", type, prune), width = 500, height = 700)
    plot(tree, uniform=TRUE,
         main = sprintf("Contraceptive Method Choice, %s, %d", type, prune))
    text(tree, use.n=TRUE, all=TRUE, cex=.8)
    dev.off()
    
    print(sprintf("Wine, %s, %d", type, prune))
    print(crossValidation(dataset_3, k, type, prune))
    tree <- testTreeConfigs(dataset_1, dataset_1, type, prune, 1)
    png(filename = sprintf("~/R/lab3/img/3_%s_%d.png", type, prune), width = 500, height = 700)
    plot(tree, uniform=TRUE,
         main = sprintf("Wine, %s, %d", type, prune))
    text(tree, use.n=TRUE, all=TRUE, cex=.8)
    dev.off()
  }
}

