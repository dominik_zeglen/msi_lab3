library('rpart')
library('class')
library('caret')
library('MASS')

# LDA
LDAclassifier <- function(train, test) {
  ldaClassifier <- lda(Class~., data = train)
  ldaPrediction <- predict(ldaClassifier, test)$class
  
  return(mean(ldaPrediction == test$Class))
}

# QDA
QDAclassifier <- function(train, test) {
  qdaClassifier <- qda(Class~., data = train)
  qdaPrediction <- predict(qdaClassifier, test)$class
  
  return(mean(qdaPrediction == test$Class))
}

# 3 - NN
NN3classifier <- function(train, test) {
  knnClassifier <- knn(train, test, train$Class, k = 3, prob = T)
  
  return(mean(knnClassifier == test$Class))
}

# 5 - NN
NN5classifier <- function(train, test) {
  knnClassifier <- knn(train, test, train$Class, k = 5, prob = T)
  
  return(mean(knnClassifier == test$Class))
}

# Place "Class" at the last column
placeClass <- function(dataset) {
  x <- c(names(dataset) != "Class")
  y <- names(dataset)
  z <- y[x]
  dataset <- dataset[, c(z, "Class")]
  
  return(dataset)
}

# Cross validation function
crossValidation <- function(dataset, folds, call) {
  accuracies <- vector("numeric")
  data <- dataset
  
  # Save Classes as factors
  data$Class <- factor(data$Class)
  
  # sample from 1 to k, nrow times (the number of observations in the data)
  data$id <- sample(1:k, nrow(data), replace = TRUE)
  list <- 1:k
  
  # Creating progress bar
  progress.bar <- create_progress_bar("text")
  progress.bar$init(k)
  
  for (i in 1:k)
  {
    # remove rows with id i from dataframe to create training set
    # select rows with id i to create test set
    trainingset <- subset(data, id %in% list[-i])
    testset <- subset(data, id %in% c(i))
    
    #Evaluate classifier for k-th fold
    fold_accuracy <- call(trainingset, testset)
    
    #Save the accuracy from the k-th fold
    accuracies <- append(accuracies, fold_accuracy)
    progress.bar$step()
  }
  
  #Calculate the final accuracy from k folds
  return(mean(accuracies))
}

#################
#               #
# Main function #
#               #
#################

dataset_1 <- placeClass(
  na.omit(
    read.csv("~/R/lab3/indianliverpatient_new.csv")))
dataset_1$Gender <- as.numeric(dataset_1$Gender)

dataset_2 <- placeClass(
  na.omit(
    read.csv("~/R/lab3/cmc_new.csv")))

dataset_3 <- placeClass(
  na.omit(
    read.csv("~/R/lab3/wine_new.csv")))

# 10 - fold CV
k <- 10

# Used classifiers
functions <- c(LDAclassifier, QDAclassifier, NN3classifier, NN5classifier)

for(classifier in functions) {
  print(crossValidation(dataset_1, k, classifier))
  print(crossValidation(dataset_2, k, classifier))
  print(crossValidation(dataset_3, k, classifier))
}

